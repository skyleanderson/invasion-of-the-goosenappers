///////////////////////////////////////////////////////////
//  CONSTANTS
///////////////////////////////////////////////////////////

//mode enum
var WORLD_SPEED = .284;//.05;
var LAYERS =
{
    BACKGROUND: 0,
    FOREGROUND: 1,
    PROJECTILE: 10,
    PLAYER    : 11
};

var PLAYER =
{
    PROJECTILE_SPEED :.6 + WORLD_SPEED,
    PROJECTILE_COOLDOWN :200,
    PLAYER_SPEED: .42,
    INVULNERABLE: 500
};


var ENEMY =
{
    COOLDOWN: 1000,
    PROJECTILE_SPEED: 0.4
};

var BOSS=
{
    COOLDOWN: 1000,
    SPECIAL_COOLDOWN: 750,
    HEALTH:50,
    PROJECTILE_SPEED: 0.4
};

var playerX, playerY;


///////////////////////////////////////////////////////////
//  PLAYER ENTITY
///////////////////////////////////////////////////////////
var PlayerEntity = me.ObjectEntity.extend(
{
    lastTime: 0,
    currTime: 0,
    millis: 0,
    font: null,
    worldPos: 0,
    fuel: 10000,
    diff: 0,

    init: function (x, y, settings)
    {
        PlayerEntity.prototype.lastTime = new Date().getTime();
        PlayerEntity.prototype.currTime = null;
        PlayerEntity.prototype.millis = 0;

        PlayerEntity.prototype.worldPos = 0;
        PlayerEntity.prototype.fuel = 10000;
        PlayerEntity.prototype.diff = 0;
        var levelname = me.levelDirector.getCurrentLevelId();
        var id = levelname.substring(5, levelname.length);
        this.levelid = parseInt(id);

        this.icon = new me.SpriteObject(0, 562, me.loader.getImage("ship_icon"));
        this.icon.resize(.5);

        PlayerEntity.prototype.font = new me.Font("Monaco, Lucida Console, monospace", 26, "#fff", "left");

        this.currentAnimation = "walk";
        this.startX = x;
        this.startY = y;

        // call the constructor
        settings.z = LAYERS.PLAYER;
        this.parent(x, y, settings);

        // set the movement speed
        this.setVelocity(100, 100);
        this.gravity = 0;

        // adjust the bounding box
        //this.updateColRect(8, 16, 0, 32);

        // set the display to follow our position on both axis
        //me.game.viewport.follow(this.pos, me.game.viewport.AXIS.BOTH);
        this.jumping = false;
        this.falling = false;
        this.renderable.alwaysUpdate = true;

        this.projectileCooldown = PLAYER.PROJECTILE_COOLDOWN;
        this.renderable.addAnimation("normal", [0]);
        this.renderable.addAnimation("left", [1]);
        this.renderable.addAnimation("right", [2]);
        this.renderable.setCurrentAnimation("normal", "normal");
        this.updateColRect(0, 64, 20, 24);

        this.invulnerable = 0;

        this.collidable = true;

    },

    update: function ()
    {
        //get a new timestamp
        PlayerEntity.prototype.currTime = new Date().getTime();
        PlayerEntity.prototype.millis = PlayerEntity.prototype.currTime - PlayerEntity.prototype.lastTime;
        if (PlayerEntity.prototype.millis > 100) PlayerEntity.prototype.millis = 16;

        PlayerEntity.prototype.fuel -= PlayerEntity.prototype.millis;
        if (PlayerEntity.prototype.fuel < 0)
        {
            //switch levels!!!
            PlayerEntity.prototype.fuel = 0;
            var levelname = me.levelDirector.getCurrentLevelId();
            var id = levelname.substring(5, levelname.length);
            id = parseInt(id) + 1;
            levelname = levelname.substring(0, 5);

            if (id == 10)
            {
                if (me.gamestat.getItemValue('boss_health') != 0)
                {
                    me.game.removeAll(true);
                    me.levelDirector.loadLevel('level8');
                    return false;
                } else
                {
                    me.game.removeAll(true);
                    me.state.change(me.state.GAMEOVER);
                }
            }
            levelname += id;

            me.game.removeAll(true);
            me.levelDirector.loadLevel(levelname);
            return false;
        }

        this.vel.x = 0;
        this.vel.y = 0;
        //left / right
        if (me.input.isKeyPressed('left'))
        {
            this.vel.x -= PLAYER.PLAYER_SPEED * PlayerEntity.prototype.millis;
            this.renderable.setCurrentAnimation("left", "left");
        }
        else if (me.input.isKeyPressed('right'))
        {
            this.vel.x += PLAYER.PLAYER_SPEED * PlayerEntity.prototype.millis;
            this.renderable.setCurrentAnimation("right", "right");
        } else
        {
            this.renderable.setCurrentAnimation("normal", "normal");
        }

        //up / down
        if (me.input.isKeyPressed('up'))
        {
            this.vel.y -= PLAYER.PLAYER_SPEED * PlayerEntity.prototype.millis;
        }
        else if (me.input.isKeyPressed('down'))
        {
            this.vel.y += PLAYER.PLAYER_SPEED * PlayerEntity.prototype.millis;
        }

        //shoot
        this.projectileCooldown -= PlayerEntity.prototype.millis;
        if (me.input.isKeyPressed('shoot') && this.projectileCooldown <= 0)
        {
            var proj = me.entityPool.newInstanceOf('projectile', this.pos.x, this.collisionBox.top + (.5 * this.collisionBox.height) - 4,
                                                  { image: "player_projectile", spritewidth: 16, spriteheight: 16, velX: PLAYER.PROJECTILE_SPEED, velY: 0, isPlayerShot:true });
            me.game.add(proj, LAYERS.PROJECTILE);
            me.game.sort();


            //ParticleEntity.prototype.spawn(10, { x: 0, y: -1 }, Math.PI, this.pos.x, this.pos.y, {});
            //spawn(10, { x: 0, y: -1 }, Math.PI, this.pos.x, this.pos.y, {});
            //spawn2()
            //for (var i = 0; i < 10; i++)
            //{
            //    var angle = 2 * Math.PI * Math.random();
            //    var part = me.entityPool.newInstanceOf('particle', this.pos.x, this.pos.y, { image: 'particle', velX: 2.2 * Math.cos(angle), velY: 2.2 * Math.sin(angle), accelX: 0, accelY: .05, duration: 750 });
            //    me.game.add(part, LAYERS.PROJECTILE);
            //}
            //me.game.sort();

            this.projectileCooldown = PLAYER.PROJECTILE_COOLDOWN;
        }
        if (this.projectileCooldown <= 0) this.projectileCooldown = 0;

        this.updateMovement();
        this.parent(this);

        //collisions
        this.invulnerable -= PlayerEntity.prototype.millis;
        if (this.invulnerable <= 0) this.invulnerable = 0;
        var res = me.game.collide(this);
        if (res && this.invulnerable == 0)
        {
            if (res.obj.type != me.game.COLLECTIBLE_ENTITY &&
                !(res.obj.type == me.game.ACTION_OBJECT && res.obj.isPlayerShot))
            {
                //take damage?
                me.gamestat.updateValue('lives', -1);
                this.renderable.flicker(PLAYER.INVULNERABLE);
                this.invulnerable = PLAYER.INVULNERABLE;
                me.game.viewport.shake(10, 100);
                if (me.gamestat.getItemValue('lives') == 0)
                {
                    //game over
                    me.state.change(me.state.GAMEOVER);
                }
            }
        }
        //move the map
        this.worldPos += WORLD_SPEED * PlayerEntity.prototype.millis;
        if (this.worldPos + me.game.viewport.width < me.game.currentLevel.cols * me.game.currentLevel.tilewidth)
        {
            PlayerEntity.prototype.diff = Math.floor(this.worldPos) - me.game.viewport.pos.x;
            me.game.viewport.pos.x = Math.floor(this.worldPos);
        } else
        {
            PlayerEntity.prototype.diff = 0;
        }

        this.pos.x += PlayerEntity.prototype.diff;

        if (this.pos.x <= me.game.viewport.pos.x) this.pos.x = me.game.viewport.pos.x + 1;
        if (this.pos.x >= me.game.viewport.pos.x + me.game.viewport.width - this.collisionBox.width) this.pos.x = me.game.viewport.pos.x + me.game.viewport.width - this.collisionBox.width - 1;

        playerX = this.collisionBox.left + .5*this.collisionBox.width;
        playerY = this.collisionBox.top + .5 * this.collisionBox.height;

        PlayerEntity.prototype.lastTime = PlayerEntity.prototype.currTime;
        return true;
    },

    draw: function (context)
    {
        //PlayerEntity.prototype.font.draw(context, "BOSS: " + me.gamestat.getItemValue('boss_health'), me.game.viewport.pos.x + 10, 3);
        //PlayerEntity.prototype.font.draw(context, "FUEL: " + this.fuel, me.game.viewport.pos.x + 750, 3);


        //draw fuel bar
        //backdrop
        var g2 = context.createLinearGradient(0, 0, 0, 20);
        g2.addColorStop(0, "#666");
        g2.addColorStop(0.5, "#ddd");
        g2.addColorStop(1, "#666");
        context.strokeStyle = g2;
        context.fillStyle = 'black';
        context.fillRect(200 + me.game.viewport.pos.x, 6, 560, 20);
        context.lineWidth = 5;
        context.strokeRect(200 + me.game.viewport.pos.x, 6, 560, 20);

        //bar

        context.lineWidth = 1;
        var g1;
        var g3;
        g1 = context.createLinearGradient(200 + me.game.viewport.pos.x, 0, 560 + 200 + me.game.viewport.pos.x, 0);
        g1.addColorStop(0, "#42b");
        g1.addColorStop(0.5, "#63c");
        g1.addColorStop(1, "#84f");
        g3 = context.createLinearGradient(200 + me.game.viewport.pos.x, 0, 560 + 200 + me.game.viewport.pos.x, 0);
        g3.addColorStop(0, "#c30");
        g3.addColorStop(0.5, "#920");
        g3.addColorStop(1, "#700");

        if (this.levelid % 2 == 0)
        {
            context.fillStyle = g1;
            context.fillRect(202 + me.game.viewport.pos.x, 8, 556 * (1-(this.fuel / 10000)), 16);
        } else
        {
            context.fillStyle = g3;
            context.fillRect(202 + me.game.viewport.pos.x, 8, 556 * (this.fuel / 10000), 16);
        }

        //lives
        for (var i = 0; i < me.gamestat.getItemValue("lives") ; i++)
        {
            this.icon.pos.x = me.game.viewport.pos.x + 30 * i;
            this.icon.draw(context);
        }
        

        //boss health meter
        if (me.gamestat.getItemValue('boss_health') != -1)
        {
            PlayerEntity.prototype.font.draw(context, "BOSS", me.game.viewport.pos.x + 465, 580);

            //backdrop
            context.strokeStyle = g2;
            context.fillStyle = 'black';
            context.fillRect(200 + me.game.viewport.pos.x, 615, 560, 20);
            context.lineWidth = 5;
            context.strokeRect(200 + me.game.viewport.pos.x, 615, 560, 20);

            //bar
            context.fillStyle = g3;
            context.fillRect(202 + me.game.viewport.pos.x, 617, 556 * (me.gamestat.getItemValue('boss_health') / BOSS.HEALTH), 16);
        }

        this.parent(context);
    }
});


///////////////////////////////////////////////////////////
//  PROJECTILE ENTITY
///////////////////////////////////////////////////////////
var ProjectileEntity = me.ObjectEntity.extend(
{

    init: function (x, y, settings)
    {
        // call the constructor
        settings.z = LAYERS.PROJECTILE;
        this.parent(x, y, settings);

        // set the movement speed
        this.setVelocity(1000, 10000);
        this.gravity = 0;
        this.vel.x = settings.velX;
        this.vel.y = settings.velY;
        this.dir = new me.Vector2d(this.vel.x, this.vel.y);
        this.speed = this.dir.length();
        this.dir.normalize();
        this.renderable.z = LAYERS.PROJECTILE;
        this.isPlayerShot = settings.isPlayerShot;
        this.type = me.game.ACTION_OBJECT;

    },

    update: function ()
    {
        this.vel.x = this.dir.x * this.speed * PlayerEntity.prototype.millis;
        this.vel.y = this.dir.y * this.speed * PlayerEntity.prototype.millis;
        this.updateMovement();
        this.parent(this);
        this.pos.x += PlayerEntity.prototype.diff;

        if ((this.vel.x != this.dir.x * this.speed * PlayerEntity.prototype.millis ||
            this.vel.y != this.dir.y * this.speed * PlayerEntity.prototype.millis) ||
            //(this.vel.x == 0 && this.vel.y == 0) ||
            !me.game.viewport.isVisible(this))
        {
            this.alive = false;
            me.game.remove(this);
        }
        return true;
    },

    onCollision: function(res, obj) 
    {
        if (this.isPlayerShot)
        {
            if (obj.type == me.game.ENEMY_TYPE)
            {
                // remove it
                this.alive = false;
                me.game.remove(this);
            }
        } else //not player shot
        {
            if (obj.name != 'enemy')
            {
                if (obj.name == 'player')
                {
                    // remove it
                    this.alive = false;
                    me.game.remove(this);
                }
            }
        }
    }
});


///////////////////////////////////////////////////////////
//  ENEMY ENTITY
///////////////////////////////////////////////////////////
var EnemyEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        this.parent(x, y, settings);

        // set the movement speed
        this.setVelocity(10, 10);
        this.gravity = 0;
        this.vel.x = settings.velX + WORLD_SPEED;
        this.vel.y = settings.velY;
        this.accel = new me.Vector2d(settings.accelX, settings.accelY);
        this.dir = new me.Vector2d(this.vel.x, this.vel.y);
        this.dir.normalize();
        this.renderable.z = LAYERS.FOREGROUND;
        this.health = settings.health;

        this.cooldown = ENEMY.COOLDOWN;
        this.type = me.game.ENEMY_TYPE;

        //stats
        me.gamestat.updateValue('total_enemies', 1);

        me.game.sort();
    },

    update: function ()
    {
        this.cooldown -= PlayerEntity.prototype.millis;
        if (this.cooldown <= 0)
        {
            //fire an enemy projectile
            var dir = new me.Vector2d(playerX, playerY);
            dir.sub(this.pos);
            dir.normalize();
            var proj = me.entityPool.newInstanceOf('projectile', this.pos.x + (.5*this.collisionBox.width), this.pos.y,
                                                  { image: "enemy_projectile", spritewidth: 16, spriteheight: 16, velX: ENEMY.PROJECTILE_SPEED * dir.x, velY: ENEMY.PROJECTILE_SPEED * dir.y, isPlayerShot: false });
            me.game.add(proj, LAYERS.PROJECTILE);
            me.game.sort();
            this.cooldown = ENEMY.COOLDOWN;
        }
        //this.vel.x = this.dir.x * PLAYER.PROJECTILE_SPEED * PlayerEntity.prototype.millis;
        //this.vel.y = this.dir.y * PLAYER.PROJECTILE_SPEED * PlayerEntity.prototype.millis;
        this.vel.x += this.accel.x;
        this.vel.y += this.accel.y;
        this.updateMovement();

        var res = me.game.collideType(this, me.game.ACTION_OBJECT);
        if (res && res.obj.isPlayerShot)
        {
            //take damage from player
            ParticleEntity.prototype.spawn(6, { x: 1, y: 0 }, 1.5 * Math.PI, this.pos.x, this.pos.y, {});

            this.health--;
            if (this.health <= 0)
            {
                //die
                me.gamestat.updateValue('enemies_killed', 1);

                this.alive = false;
                me.game.remove(this);
            }
        }

        this.parent(this);

        if (!me.game.viewport.isVisible(this))
        {
            this.alive = false;
            me.game.remove(this);
        }
    }
});



///////////////////////////////////////////////////////////
//  BOSS ENTITY
///////////////////////////////////////////////////////////
var BossEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        this.parent(x, y, settings);

        // set the movement speed
        this.setVelocity(10, 10);
        this.gravity = 0;
        this.renderable.z = LAYERS.FOREGROUND;

        //only set health the first time
        if (me.gamestat.getItemValue('boss_health') == -1)
        {
            me.gamestat.setValue('boss_health', BOSS.HEALTH);
            me.gamestat.updateValue('total_enemies', 1);
        }
        this.health = me.gamestat.getItemValue('boss_health');

        this.cooldown = BOSS.COOLDOWN;
        this.specialCooldown = BOSS.SPECIAL_COOLDOWN;
        this.type = me.game.ENEMY_TYPE;
        //this.updateColRect(8, 32, 8, 32);
        this.renderable.resize(2);

        this.pos.x = 480 + Math.cos(0) * 150;
        this.pos.y = 320 + Math.sin(0) * 150;

        me.game.sort();
    },

    update: function ()
    {
        this.cooldown -= PlayerEntity.prototype.millis;
        this.specialCooldown -= PlayerEntity.prototype.millis;
        if (this.cooldown <= 0)
        {
            //fire an enemy projectile
            var dir = new me.Vector2d(playerX, playerY);
            dir.sub(this.pos);
            dir.normalize();
            var proj = me.entityPool.newInstanceOf('projectile', this.pos.x + (.5 * this.collisionBox.width), this.pos.y,
                                                  { image: "enemy_projectile", spritewidth: 16, spriteheight: 16, velX: BOSS.PROJECTILE_SPEED * dir.x, velY: BOSS.PROJECTILE_SPEED * dir.y, isPlayerShot: false });
            me.game.add(proj, LAYERS.PROJECTILE);
            me.game.sort();
            this.cooldown = BOSS.COOLDOWN;
        }

        if (this.specialCooldown <= 0)
        {
            //fire a ring of projectiles
            this.specialCooldown = BOSS.SPECIAL_COOLDOWN;

            for (var i = 0; i < 16; i++)
            {
                var angle = (.0625 * i) * (2 * Math.PI);

                var proj = me.entityPool.newInstanceOf('projectile', this.pos.x + (.5 * this.collisionBox.width), this.pos.y,
                                                      { image: "enemy_projectile", spritewidth: 16, spriteheight: 16, velX: BOSS.PROJECTILE_SPEED * Math.cos(angle), velY: BOSS.PROJECTILE_SPEED * Math.sin(angle), isPlayerShot: false });
                me.game.add(proj, LAYERS.PROJECTILE);
            }
            me.game.sort();
            this.specialCooldown = BOSS.SPECIAL_COOLDOWN;
        }
        this.pos.x = 480 + Math.cos(2*Math.PI*(PlayerEntity.prototype.fuel / 10000)) * 150;
        this.pos.y = 320 + Math.sin(2 * Math.PI * (PlayerEntity.prototype.fuel / 10000)) * 150;

        var res = me.game.collideType(this, me.game.ACTION_OBJECT);
        if (res && res.obj.isPlayerShot)
        {
            //take damage from player
            ParticleEntity.prototype.spawn(6, { x: 1, y: 0 }, 1.5 * Math.PI, this.pos.x, this.pos.y, {});

            this.health--;
            me.gamestat.setValue('boss_health', this.health);
            if (this.health <= 0)
            {
                //die
                me.gamestat.setValue('boss_health', 0);
                me.gamestat.updateValue('enemies_killed', 1);

                this.alive = false;
                me.game.remove(this);
                me.state.change(me.state.GAMEOVER);
            }
        }

        this.parent(this);

        if (!me.game.viewport.isVisible(this))
        {
            this.alive = false;
            me.game.remove(this);
        }
    }
});

///////////////////////////////////////////////////////////
//  GOOSE ENTITY
///////////////////////////////////////////////////////////
var GooseEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        this.parent(x, y, settings);

        // set the movement speed
        this.setVelocity(10, 10);
        this.gravity = 0;
        this.vel.x = settings.velX;
        this.vel.y = settings.velY;
        this.accel = new me.Vector2d(settings.accelX, settings.accelY);
        this.dir = new me.Vector2d(this.vel.x, this.vel.y);
        this.dir.normalize();
        this.renderable.z = LAYERS.FOREGROUND;

        this.renderable.addAnimation("fly", [0, 1, 2, 1, 1, 0, 0, 0, 1, 1, 2, 1]);
        this.renderable.setCurrentAnimation("fly", "fly");
        this.renderable.setAnimationFrame(Math.floor(Math.random() * 11));
        this.type = me.game.ENEMY_TYPE;
        this.updateColRect(0, 64, 20, 24);

        //stats
        me.gamestat.updateValue('total_geese', 1);


        me.game.sort(); 
    },

    update: function ()
    {
        this.vel.x += this.accel.x;
        this.vel.y += this.accel.y;
        this.updateMovement();
        var res = me.game.collide(this);
        if (res && res.obj.type != me.game.ENEMY_TYPE)
        {
            //goose death
            me.gamestat.updateValue('geese_killed', 1);

            ParticleEntity.prototype.spawn(15, { x: 1, y: 0 }, 2 * Math.PI, this.pos.x, this.pos.y, {});
            this.alive = false;
            me.game.remove(this);
        }

        this.parent(this);

        if (!me.game.viewport.isVisible(this))
        {
            this.alive = false;
            me.game.remove(this);
        }
    }
});



///////////////////////////////////////////////////////////
//  LIfe ENTITY
///////////////////////////////////////////////////////////
var LifeEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // call the constructor
        settings.image = 'ship_icon';
        settings.spritewidth = 64;
        settings.spriteheight = 64;
        this.parent(x, y, settings);
        this.renderable.resize(.5);

        // set the movement speed
        this.setVelocity(0, 0);
        this.gravity = 0;
        this.renderable.z = LAYERS.FOREGROUND;
        this.type = me.game.COLLECTIBLE_ENTITY;
        me.game.sort();
    },

    update: function ()
    {
        var res = me.game.collide(this);
        if (res && res.obj.name == 'player')
        {
            this.alive = false;
            me.game.remove(this);
            me.gamestat.updateValue("lives", 1);
        }

        this.parent(this);

        if (!me.game.viewport.isVisible(this))
        {
            this.alive = false;
            me.game.remove(this);
        }
    }
});

///////////////////////////////////////////////////////////
//  PARTICLE ENTITY
///////////////////////////////////////////////////////////
var ParticleEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        this.parent(x, y, settings);
        this.tweener = new me.Tween(this);
        this.time = 0;
        this.tweener.to({ time: 1 }, settings.duration);
        this.vel.x = settings.velX;
        this.vel.y = settings.velY;
        this.accel = new me.Vector2d(settings.accelX, settings.accelY);
        this.gravity = 0;
        this.collidable = false;

        this.tweener.onComplete(this.destroy);
        this.tweener.start();
    },

    update: function ()
    {
        //this.time should be updated by the tweener
        this.vel.x += this.accel.x;
        this.vel.y += this.accel.y;
        this.renderable.setOpacity(1 - (.5 * this.time));
        this.renderable.resize(1.5 - this.time);
        this.updateMovement();
        this.parent(this);
    },

    destroy: function ()
    {
        this.alive = false;
        me.game.remove(this);
    },

    spawn: function (amount, dir, spread, x, y, settings)
    {
        //spawn(10, { x: 1, y: 1 }, 2 * Math.PI, this.x, this.y, { image: 'particle', spritewidth: 8, spriteheight: 8, velX: 2.2 * Math.cos(angle), velY: 2.2 * Math.sin(angle), accelX: 0, accelY: .05, duration: 750 });

        var unit = new me.Vector2d(1, 0);
        for (var i = 0; i < amount; i++)
        {
            var angle = unit.angle(dir);
            angle += (.5 * spread) - (spread * Math.random());
            var part = me.entityPool.newInstanceOf('particle', x, y, { image: 'particle', spritewidth: 8, spriteheight: 8, velX: WORLD_SPEED+ Math.random() * 2.2 * Math.cos(angle), velY: Math.random() * 2.2 * Math.sin(angle), accelX: 0, accelY: .05, duration: 750 });
            me.game.add(part, LAYERS.PROJECTILE);
        }
        me.game.sort();
    }
});
