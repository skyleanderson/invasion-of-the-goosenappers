﻿game.GameOverScreen = me.ScreenObject.extend({
    /**     
     *  action to perform on state change
     */
    onResetEvent: function ()
    {
        this.title = new me.Font("Lucida Console, monospace", 36, "#fff", "left");
        this.prompt = new me.Font("Lucida Console, monospace", 16, "#fff", "left");
        me.game.add(new me.ColorLayer("bgc", "#000000", 1));
        this.bg = new me.ImageLayer('bg', 640, 640, 'gameover_bg', 0);
        this.bg.repeat = 'repeat';
        me.game.add(this.bg, 0);
        this.cooldown = 30;
        this.showPrompt = true;
    },

    init: function ()
    {
        this.parent(true, true);
    },

    draw: function (context)
    {
        me.video.clearSurface(context, "#000");
        this.parent(this);
        //this.bg.draw(context);

        var pat = context.createPattern(this.bg.image, "repeat");
        context.fillStyle = pat
        context.fillRect(0, 0, 960, 640);

        var size = this.title.measureText(context, "Game Over, Thanks for Playing!");
        this.title.draw(context, "Game Over, Thanks for Playing!", (me.video.getWidth() * .5) - (size.width * .5), 100);


        var geese  = "Geese Saved : " + (me.gamestat.getItemValue('total_geese') - me.gamestat.getItemValue('geese_killed')) + 
            " / " + (me.gamestat.getItemValue('total_geese'));
        var enemies = "Enemies Killed : " + (me.gamestat.getItemValue('enemies_killed')) + " / " + me.gamestat.getItemValue('total_enemies');

        var rank = "";
        if (me.gamestat.getItemValue('boss_health') == 0) rank += "Victorious ";

        if (me.gamestat.getItemValue('enemies_killed') >= 20) rank += "Murderous ";

        if (me.gamestat.getItemValue('geese_killed') >= 50)
        {
            rank += "Goose Genecidist"
        } else if (me.gamestat.getItemValue('geese_killed') == 0) 
        {
            rank += "Goose Messiah";
        } else if (me.gamestat.getItemValue('geese_killed') < 10)
        {
            rank += "Goose Savior";
        } else
        {
            rank += "Governor of Geese";
        }

        //stats
        var size = this.title.measureText(context, "STATS");
        this.title.draw(context, "STATS", (me.video.getWidth() * .5) - (size.width * .5), 180);

        //geese
        size = this.prompt.measureText(context, geese);
        this.prompt.draw(context, geese, (me.video.getWidth() * .5) - (size.width * .5), 240);

        //enemies
        size = this.prompt.measureText(context, enemies);
        this.prompt.draw(context, enemies, (me.video.getWidth() * .5) - (size.width * .5), 270);

        //rank
        var size = this.title.measureText(context, "RANK");
        this.title.draw(context, "RANK", (me.video.getWidth() * .5) - (size.width * .5), 330);

        size = this.prompt.measureText(context, rank);
        this.prompt.draw(context, rank, (me.video.getWidth() * .5) - (size.width * .5), 390);

        size = this.prompt.measureText(context, "PRESS ENTER TO CONTINUE");
        this.prompt.draw(context, "PRESS ENTER TO CONTINUE", (me.video.getWidth() * .5) - (size.width * .5), 540);

        return true;
    },

    update: function ()
    {
        if (me.input.isKeyPressed("enter"))
        {
            //me.audio.play("start");
            me.state.change(me.state.MENU);
        }
        return true;
    },

});