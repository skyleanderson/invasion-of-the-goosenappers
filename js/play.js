game.PlayScreen = me.ScreenObject.extend({
	/**	
	 *  action to perform on state change
	 */
    onResetEvent: function ()
    {
        me.gamestat.setValue('boss_health', -1);
        me.gamestat.setValue('lives', 3);
        me.gamestat.setValue('total_geese', 0);
        me.gamestat.setValue('geese_killed', 0);
        me.gamestat.setValue('total_enemies', 0);
        me.gamestat.setValue('enemies_killed', 0);
        me.levelDirector.loadLevel("level1");
	},
	
	
	/**	
	 *  action to perform when leaving this screen (state change)
	 */
	onDestroyEvent: function() {
	  ; // TODO
	}
});
