game.TitleScreen = me.ScreenObject.extend({
    /**     
     *  action to perform on state change
     */
    onResetEvent: function ()
    {
        this.title = new me.Font("Lucida Console, monospace", 36, "#fff", "left");
        this.prompt = new me.Font("Lucida Console, monospace", 16, "#bbb", "left");
        me.game.add(new me.ColorLayer("bgc", "#000000", 1));
        this.bg = new me.ImageLayer('bg', 640, 640, 'title_bg', 0);
        this.bg.repeat = 'repeat';
        me.game.add(this.bg, 0);
        this.cooldown = 30;
        this.showPrompt = true;
    },

    init: function ()
    {
        this.parent(true, true);
    },

    draw: function (context)
    {
        me.video.clearSurface(context, "#000");
        this.parent(this);
        //this.bg.draw(context);

        var pat = context.createPattern(this.bg.image, "repeat");
        context.fillStyle = pat
        context.fillRect(0,0,960,640);

        var size = this.title.measureText(context, "Invasion of the GooseNappers");
        this.title.draw(context, "Invasion of the GooseNappers", (me.video.getWidth() * .5) - (size.width * .5), 100);

        if (me.gamestat.getItemValue("lastScore") > 0)
        {
            size = this.prompt.measureText(context, "LAST: " + me.gamestat.getItemValue("lastScore") + " HIGH: " + me.gamestat.getItemValue("highScore"));
            this.prompt.draw(context, "LAST: " + me.gamestat.getItemValue("lastScore") + " HIGH: " + me.gamestat.getItemValue("highScore"), (me.video.getWidth() * .5) - (size.width * .5), 150);
        }

        if (this.showPrompt)
        {
            size = this.prompt.measureText(context, "PRESS ENTER TO BEGIN");
            this.prompt.draw(context, "PRESS ENTER TO BEGIN", (me.video.getWidth() * .5) - (size.width * .5), 330);
        }

        return true;
    },

    update: function ()
    {
        this.cooldown--;
        if (this.cooldown <= 0)
        {
            if (!this.showPrompt) this.cooldown = 60;
            else this.cooldown = 30;
            this.showPrompt = !this.showPrompt;
        }
        if (me.input.isKeyPressed("enter"))
        {
            //me.audio.play("start");
            me.state.change(me.state.PLAY);
        }
        this.bg1x += .5;
        this.bg2x += .75;
        this.bg1 
        return true;
    },

    /**     
     *  action to perform when leaving this screen (state change)
     */
    onDestroyEvent: function ()
    {
        ; // TODO
    }
});