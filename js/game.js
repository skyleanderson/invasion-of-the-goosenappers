
/* Game namespace */
var game = {
    // Run on page load.
    "onload" : function () {
        // Initialize the video.
        if (!me.video.init("screen", 960, 640, true, 'auto')) {
            alert("Your browser does not support HTML5 canvas.");
            return;
        }
		
		// add "#debug" to the URL to enable the debug Panel
		if (document.location.hash === "#debug") {
			window.onReady(function () {
				me.plugin.register.defer(debugPanel, "debug");
			});
		}

        // Initialize the audio.
        me.audio.init("mp3,ogg");

        // Set a callback to run when loading is complete.
        me.loader.onload = this.loaded.bind(this);
     
        // Load the resources.
        me.loader.preload(game.resources);

        // Initialize melonJS and display a loading screen.
        me.state.change(me.state.LOADING);
    },



    // Run on game resources loaded.
    "loaded": function ()
    {
        //me.debug.renderHitBox = true;
        //game entities
        me.entityPool.add("player", PlayerEntity);
        me.entityPool.add("projectile", ProjectileEntity);
        me.entityPool.add("enemy", EnemyEntity);
        me.entityPool.add("boss", BossEntity);
        me.entityPool.add("goose", GooseEntity);
        me.entityPool.add("particle", ParticleEntity);
        me.entityPool.add("life", LifeEntity);

        //states
        me.state.set(me.state.MENU, new game.TitleScreen());
        me.state.set(me.state.PLAY, new game.PlayScreen());
        me.state.set(me.state.GAMEOVER, new game.GameOverScreen());

        //statistics
        me.gamestat.add("lives", 3);
        me.gamestat.add("boss_health", -1);
        me.gamestat.add("total_geese", 0);
        me.gamestat.add("geese_killed", 0);
        me.gamestat.add("total_enemies", 0);
        me.gamestat.add("enemies_killed", 0);

        //controls
        me.input.bindKey(me.input.KEY.UP, "up");
        me.input.bindKey(me.input.KEY.DOWN, "down");
        me.input.bindKey(me.input.KEY.LEFT, "left");
        me.input.bindKey(me.input.KEY.RIGHT, "right");

        me.input.bindKey(me.input.KEY.SPACE, "shoot");
        me.input.bindKey(me.input.KEY.ENTER, "enter", true);

        // Start the game.
        me.state.change(me.state.MENU);
    }
};
