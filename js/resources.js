game.resources = [

	/* Graphics. 
	 * @example
	 * {name: "example", type:"image", src: "data/img/example.png"},
	 */
    { name: "tilemap", type: "image", src: "res/img/tilemap.png" },
    { name: "metatiles32x32", type: "image", src: "res/img/metatiles32x32.png" },
    { name: "title_bg", type: "image", src: "res/img/title_bg.png" },
    { name: "gameover_bg", type: "image", src: "res/img/gameover_bg.png" },
    { name: "bg1", type: "image", src: "res/img/bg1.png" },
    { name: "bg2", type: "image", src: "res/img/bg2.png" },
    { name: "goose_bg1", type: "image", src: "res/img/goose_bg1.png" },
    { name: "goose_bg2", type: "image", src: "res/img/goose_bg2.png" },
    { name: "ship_icon", type: "image", src: "res/img/ship_icon.png" },
    { name: "player", type: "image", src: "res/img/player.png" },
    { name: "goose", type: "image", src: "res/img/goose.png" },
    { name: "player_projectile", type: "image", src: "res/img/player_projectile.png" },
    { name: "particle", type: "image", src: "res/img/particle.png" },

    { name: "squid", type: "image", src: "res/img/squid.png" },
    { name: "boss", type: "image", src: "res/img/boss.png" },
    { name: "enemy_projectile", type: "image", src: "res/img/enemy_projectile.png" },

	/* Maps. 
	 * @example
	 * {name: "example01", type: "tmx", src: "data/map/example01.tmx"},
	 * {name: "example01", type: "tmx", src: "data/map/example01.json"},
 	 */
	{ name: "level1", type: "tmx", src: "res/level1.tmx" },
    { name: "level2", type: "tmx", src: "res/level2.tmx" },
    { name: "level3", type: "tmx", src: "res/level3.tmx" },
    { name: "level4", type: "tmx", src: "res/level4.tmx" },
    { name: "level5", type: "tmx", src: "res/level5.tmx" },
    { name: "level6", type: "tmx", src: "res/level6.tmx" },
    { name: "level7", type: "tmx", src: "res/level7.tmx" },
    { name: "level8", type: "tmx", src: "res/level8.tmx" },
    { name: "level9", type: "tmx", src: "res/level9.tmx" }

	/* Background music. 
	 * @example
	 * {name: "example_bgm", type: "audio", src: "data/bgm/", channel : 1},
	 */	
	
	/* Sound effects. 
	 * @example
	 * {name: "example_sfx", type: "audio", src: "data/sfx/", channel : 2}
	 */
];
