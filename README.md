Title TBD
-------------------------------------------------------------------------------

Entry to the Ludum Dare 27 competition
by:
Kyle Anderson

using:
javascript
melonJS
GraphicsGale
ASEPRITE
iNudge
Audacity

-------------------------------------------------------------------------------
Copyright (C) 2013 Kyle Anderson

-------------------------------------------------------------------------------
melonJS:
Copyright (C) 2011 - 2013, Olivier Biot, Jason Oster
melonJS is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php)